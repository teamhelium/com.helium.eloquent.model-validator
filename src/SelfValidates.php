<?php

namespace Helium\EloquentValidator;

use Illuminate\Database\Eloquent\Model;
use Validator;

trait SelfValidates
{
	protected $validateOnSave = true;

	protected $rules = [];

	protected $messages = [];

	public function validate()
	{
		$rules = $this->rules;
		$messages = $this->messages;

		if (method_exists($this, 'rules')) {
			$rules = $this->rules();
		}

		if (method_exists($this, 'messages')) {
			$messages = $this->messages();
		}

		$v = Validator::make($this->attributesToArray(), $rules, $messages);

		return $v->validate();
	}

	public function validatesOnSave()
	{
		return $this->validateOnSave;
	}

	public static function bootSelfValidates()
	{
		self::saving(function (Model $model) {
			if ($model->validatesOnSave())
			{
				$model->validate();
			}
		});
	}
}
