# Change Log

## 1.1.0
##### 01-09-2020

- Updated namespace (breaking change)

## 1.0.3
##### 10-30-2019

- Fixed `messages()` reference error

## 1.0.2
##### 10-12-2019

- Changed `getRules()` to `rules()`
- Added support for `$messages` and `messages()`

## 1.0.1
##### 09-18-2019

- Added `$rules` property
- Renamed `rules()` to `getRules()`
- Defined default implementation for `getRules()`

## 1.0.0
##### 09-16-2019

- Initial release